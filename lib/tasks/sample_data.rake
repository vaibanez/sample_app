namespace :db do
  desc "Llena la base de datos con datos de ejemplo"
  task populate: :environment do
    hacer_usuarios
    hacer_microposts
    hacer_relaciones
  end
end

def hacer_usuarios
  Usuario.create!(nombre: "Example User",email: "example@railstutorial.org", password: "foobar", password_confirmation: "foobar", admin: true)
    99.times do |n|
      nombre  = Faker::Name.name
      email = "example-#{n+1}@railstutorial.org"
      password  = "password"
      Usuario.create!(nombre: nombre, email: email, password: password, password_confirmation: password)
    end
end

def hacer_microposts
  usuarios = Usuario.limit(6)
    50.times do
      content = Faker::Lorem.sentence(5)
      usuarios.each { |usuario| usuario.microposts.create!(content: content) }
    end  
end

def hacer_relaciones
  usuarios = Usuario.all
  usuario = usuarios.first
  usuarios_seguido = usuarios[2..50]
  seguidores = usuarios[3..40]
  usuarios_seguido.each {|seguido| usuario.seguir!(seguido)}
  seguidores.each {|seguidor| seguidor.seguir!(usuario)}
end
