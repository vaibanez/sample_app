require 'spec_helper'

describe "PaginasMicroposts" do
  subject { page }

  let(:usuario) { FactoryGirl.create(:usuario) }
  before { sign_in usuario }

  describe "creacion del micropost" do
    before { visit root_path }

    describe "con informacion invalida" do

      it "no deberia crear un micropost" do
        expect { click_button "Publicar" }.not_to change(Micropost, :count)
      end

      describe "mensajes de error" do
        before { click_button "Publicar" }
        it { should have_content('error') }
      end
    end

    describe "con informacion valida" do

      before { fill_in 'micropost_content', with: "Lorem ipsum" }
      it "deberia crear un micropost" do
        expect { click_button "Publicar" }.to change(Micropost, :count).by(1)
      end
    end
  end

  describe "destruccion de microposts" do
    before { FactoryGirl.create(:micropost, usuario: usuario) }

    describe "como usuario correcto" do
      before { visit root_path }

      it "deberia borrar un micropost" do
        expect { click_link "borrar" }.to change(Micropost, :count).by(-1)
      end
    end
  end
end
