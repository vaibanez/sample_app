require 'spec_helper'

describe "PaginasEstaticas" do
  subject { page }

  describe "Home page" do
 	before { visit root_path }

 	  it { should have_content('Aplicacion de ejemplo') }
    it { should have_title(full_titulo('')) }
    it { should_not have_title('| Inicio') }

    describe "para usuarios que han ingresado" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      before do
        FactoryGirl.create(:micropost, usuario: usuario, content: "Lorem ipsum")
        FactoryGirl.create(:micropost, usuario: usuario, content: "Dolor sit amet")
        sign_in usuario
        visit root_path
      end

      it "deberia mostrar el pensamiento del usuario" do
        usuario.feed.each do |item|
          expect(page).to have_selector("li##{item.id}", text: item.content)
        end
      end

      describe "seguidor/siguiendo conteo" do
        let(:otro_usuario) { FactoryGirl.create(:usuario) }
        before do
          otro_usuario.seguir!(usuario)
          visit root_path
        end

        it { should have_link("0 siguiendo", href: siguiendo_usuario_path(usuario)) }
        it { should have_link("1 seguidores", href: seguidores_usuario_path(usuario)) }
      end
    end

 	#it "Deberia tener el contenido 'Aplicacion de ejemplo'" do
 		#visit '/paginas_estaticas/home'
 		#visit root_path
 	#	expect(page).to have_content('Aplicacion de ejemplo')
 	#end

 	#it "Deberia tener de Titulo 'Inicio'" do
 		#visit '/paginas_estaticas/home'
 		#visit root_path
 	#	expect(page).to have_title("Tutorial Ruby on Rails, Aplicacion de Ejemplo")
 	#end

 	#it "No deberia tener un titulo personalizado" do
 		#visit '/paginas_estaticas/home'
 		#visit root_path
 	#	expect(page).not_to have_title('| Inicio')
 	#end
  end

  describe "Help page" do
  	before { visit ayuda_path}
 	  it { should have_content('Ayuda') }
    it { should have_title(full_titulo('Ayuda')) }
 	# it "Deberia tener el contenido 'Ayuda'" do
 	# 	#visit '/paginas_estaticas/help'
 	# 	visit ayuda_path
 	# 	expect(page).to have_content('Ayuda')
 	# end

 	# it "Deberia tener el Titulo 'Ayuda'" do
 	# 	#visit '/paginas_estaticas/help'
 	# 	visit ayuda_path
 	# 	expect(page).to have_title("Tutorial Ruby on Rails, Aplicacion de Ejemplo | Ayuda")
 	# end
  end

  describe "About page" do
  	before {visit acerca_path}
 	  it { should have_content('Acerca de Nosotros') }
    it { should have_title(full_titulo('Nosotros')) }
 	# it "Deberia tener el contenido 'Acerca de Nosotros'" do
 	# 	#visit '/paginas_estaticas/about'
 	# 	visit acerca_path
 	# 	expect(page).to have_content('Acerca de Nosotros')
 	# end

 	# it "Deberia tener el titulo 'Nosotros'" do
 	# 	#visit '/paginas_estaticas/about'
 	# 	visit acerca_path
 	# 	expect(page).to have_title("Tutorial Ruby on Rails, Aplicacion de Ejemplo | Nosotros")
 	# end
  end

  describe "Contact page" do
  	before {visit contacto_path}
  	it { should have_content('Contacto') }
    it { should have_title(full_titulo('Contacto')) }
 	# it "Deberia tener el contenido 'Contacto'" do
 	# 	#visit 'paginas_estaticas/contact'
 	# 	visit contacto_path
 	# 	expect(page).to have_content('Contacto')
 	# end

 	# it "Deberia tener el titulo 'Cotacto'" do
 	# 	#visit 'paginas_estaticas/contact'
 	# 	visit contacto_path
 	# 	expect(page).to have_title('Contacto')
 	# end
  end
end
