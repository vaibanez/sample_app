require 'spec_helper'

describe "PaginasAutenticacions" do
  subject { page }

  describe "Pagina de Ingreso" do
    before { visit ingreso_path }

    it { should have_content('Ingresar') }
    it { should have_title('Ingresar') }
  end

  describe "con informacion valida" do
    let(:usuario) { FactoryGirl.create(:usuario) }
    before {sign_in usuario}

    it { should have_title(usuario.nombre)}
    it { should have_link('Usuarios',       href: usuarios_path) }
    it { should have_link('Perfil',         href: usuario_path(usuario)) }
    it { should have_link('Herramientas',   href: edit_usuario_path(usuario)) }
    it { should have_link('Salir',          href: salida_path) }
    it { should_not have_link('Ingresar',   href: ingreso_path) }
  end

  describe "Ingreso" do
  	before { visit ingreso_path }

    describe "Con informacion invalida" do
      before { click_button "Ingresar" }

      it { should have_title('Ingresar') }
      it { should have_selector('div.alert.alert-error') }

      describe "Despues de visitar otra pagina" do
        before { click_link "Inicio" }
        it { should_not have_selector('div.alert.alert-error') }
      end

    end

    describe "Con informacion valida" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      before do
        fill_in "Email",    with: usuario.email.upcase
        fill_in "Password", with: usuario.password
        click_button "Ingresar"
      end

      it { should have_title(usuario.nombre) }
      it { should have_link('Perfil',   href: usuario_path(usuario)) }
      it { should have_link('Salir',    href: salida_path) }
      it { should_not have_link('Ingresar', href: ingreso_path) }

      describe "Seguido de salir" do
        before { click_link "Salir" }
        it { should have_link('Ingresar') }
      end
    end
  end

  describe "autorizacion" do

    describe "para usuarios que no han ingresado" do
      let(:usuario) { FactoryGirl.create(:usuario) }

      describe "cuando se intenta visitar una pagina protegida" do
        before do
          visit edit_usuario_path(usuario)
          fill_in "Email",    with: usuario.email
          fill_in "Password", with: usuario.password
          click_button "Ingresar"
        end

        describe "despues de ingresar" do
          it "debe ir a la pagina protegida deseada" do
            expect(page).to have_title('Editar usuario')
          end
        end
      end

      describe "en el usuarios controller" do

        describe "visitando la pagina de edicion" do
          before { visit edit_usuario_path(usuario) }
          it { should have_title('Ingresar') }
        end

        describe "enviando hacia la accion de actualizacion" do
          before { patch usuario_path(usuario) }
          specify { expect(response).to redirect_to(ingreso_path) }
        end

        describe "visitando el usuario index" do
          before { visit usuarios_path }
          it { should have_title('Ingresar') }
        end

        describe "visitando la pagina siguiendo" do
          before { visit siguiendo_usuario_path(usuario) }
          it { should have_title('Ingresar') }
        end

        describe "visitando la pagina seguidores" do
          before { visit seguidores_usuario_path(usuario) }
          it { should have_title('Ingresar') }
        end

      end

      describe "en el Microposts controller" do

        describe "enviando la accion create" do
          before { post microposts_path }
          specify { expect(response).to redirect_to(ingreso_path) }
        end

        describe "enviando la accion destroy" do
          before { delete micropost_path(FactoryGirl.create(:micropost)) }
          specify { expect(response).to redirect_to(ingreso_path) }
        end
      end

      describe "en el relacions controller" do
        describe "enviando la accion create" do
          before { post relacions_path }
          specify { expect(response).to redirect_to(ingreso_path) }
        end

        describe "enviando la accion destroy" do
          before { delete relacion_path(1) }
          specify { expect(response).to redirect_to(ingreso_path) }
        end
      end

    end

    describe "como usuario erroneo" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      let(:usuario_erroneo) { FactoryGirl.create(:usuario, email: "erroneo@example.com") }
      before { sign_in usuario, no_capybara: true }

      describe "enviando una solicitud GET a la accion Usuarios#edit" do
        before { get edit_usuario_path(usuario_erroneo) }
        specify { expect(response.body).not_to match(full_titulo('Editar usuario')) }
        specify { expect(response).to redirect_to(root_url) }
      end

      describe "enviando una solicitud PATCH hacia la accion Usuarios#update" do
        before { patch usuario_path(usuario_erroneo) }
        specify { expect(response).to redirect_to(root_url) }
      end
    end

    describe "como usuario no administrador" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      let(:non_admin) { FactoryGirl.create(:usuario) }

      before { sign_in non_admin, no_capybara: true }

      describe "enviando una solicitud DELETE hacia la accion Usuarios#destroy" do
        before { delete usuario_path(usuario) }
        specify { expect(response).to redirect_to(root_url) }
      end
    end
  end
end
