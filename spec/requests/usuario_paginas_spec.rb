require 'spec_helper'

describe "UsuarioPaginas" do
  subject {page}

  describe "index" do
    let(:usuario) { FactoryGirl.create(:usuario)}

    before do
      #sign_in FactoryGirl.create(:usuario)
      sign_in usuario
      #FactoryGirl.create(:usuario, nombre: "Bob", email: "bob@example.com")
      #FactoryGirl.create(:usuario, nombre: "Ben", email: "ben@example.com")
      visit usuarios_path
    end

    it { should have_title('Usuarios') }
    it { should have_content('Usuarios') }

    it "deberia listar cada usuario" do
      Usuario.all.each do |usuario|
        expect(page).to have_selector('li', text: usuario.nombre)
      end
    end

    describe "paginacion" do

      before(:all) { 30.times { FactoryGirl.create(:usuario) } }
      after(:all)  { Usuario.delete_all }

      it { should have_selector('div.pagination') }

      it "should list each user" do
        Usuario.paginate(page: 1).each do |usuario|
          expect(page).to have_selector('li', text: usuario.nombre)
        end
      end
    end

    describe "links borrar" do

      it { should_not have_link('borrar') }

      describe "como un usuario administrador" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          sign_in admin
          visit usuarios_path
        end

        it { should have_link('borrar', href: usuario_path(Usuario.first)) }
        it "deberia ser accesible para borrar otro usuario" do
          expect do
            click_link('borrar', match: :first)
          end.to change(Usuario, :count).by(-1)
        end
        it { should_not have_link('borrar', href: usuario_path(admin)) }
      end
    end
  end

  describe 'Pagina de perfil' do
  	let(:usuario) { FactoryGirl.create(:usuario)}
    let!(:m1) { FactoryGirl.create(:micropost, usuario: usuario, content: "Foo") }
    let!(:m2) { FactoryGirl.create(:micropost, usuario: usuario, content: "Bar") }

  	before { visit usuario_path(usuario)}

  	it {should have_content(usuario.nombre)}
  	it {should have_title(usuario.nombre)}

    describe "microposts" do
      it { should have_content(m1.content) }
      it { should have_content(m2.content) }
      it { should have_content(usuario.microposts.count) }
    end

    describe "botones seguir/noseguir" do
      let(:otro_usuario) { FactoryGirl.create(:usuario) }
      before { sign_in usuario }

      describe "siguiendo un usuario" do
        before { visit usuario_path(otro_usuario) }

        it "debe incrementar el conteo de los seguidores del usuario" do
          expect do
            click_button "Seguir"
          end.to change(usuario.usuarios_seguido, :count).by(1)
        end

        it "debe incrementar el conteo de los seguidores del otro usuario" do
          expect do
            click_button "Seguir"
          end.to change(otro_usuario.seguidores, :count).by(1)
        end

        describe "alternando el boton" do
          before { click_button "Seguir" }
          it { should have_xpath("//input[@value='No seguir']") }
        end
      end

      describe "no siguiendo a un usuario" do
        before do
          usuario.seguir!(otro_usuario)
          visit usuario_path(otro_usuario)
        end

        it "debe disminuir el conteo de seguidos del usuario" do
          expect do
            click_button "No seguir"
          end.to change(usuario.usuarios_seguido, :count).by(-1)
        end

        it "debe disminuir el conteo de seguidores del otro usuario" do
          expect do
            click_button "No seguir"
          end.to change(otro_usuario.seguidores, :count).by(-1)
        end

        describe "alternando el boton" do
          before { click_button "No seguir" }
          it { should have_xpath("//input[@value='Seguir']") }
        end
      end
    end

  end

  describe 'Pagina para darse de alta' do
  	before {visit alta_path}
  	it { should have_content('Darse de Alta') }
    it { should have_title(full_titulo('Darse de Alta')) }
  end

  describe 'Alta' do
  	before {visit alta_path}
  	let(:submit) {"Crear mi cuenta"}

  	describe "Con informacion invalida" do
  	 	it "No debe crear un usuario" do
  	 		expect { click_button submit }.not_to change(Usuario, :count)
  	 	end
  	end

  	describe "Con informacion valida" do
  		before do
       	fill_in "Nombre",       with: "Example User"
       	fill_in "Email",        with: "user@example.com"
       	fill_in "Password",     with: "foobar"
       	fill_in "Confirmacion", with: "foobar"
      end

      it "Debe crear un usuario" do
      	expect {click_button submit}.to change(Usuario, :count).by(1)
      end

      describe "Despues de guardar el usuario" do
        before { click_button submit }
        let(:usuario) { Usuario.find_by(email: 'user@example.com') }

        it { should have_link('Salir') }
        it { should have_title(usuario.nombre) }
        it { should have_selector('div.alert.alert-success', text: 'Bienvenido') }
      end
  	end
  end

  describe "Editar" do
    let(:usuario) {FactoryGirl.create(:usuario)}
    before {
      sign_in usuario
      visit edit_usuario_path(usuario)
    }

    describe "Pagina" do
      it {should have_content("Actualiza tu perfil")}
      it {should have_title("Editar usuario")}
      it {should have_link('cambiar', href: 'http://gravatar.com/emails' )}
    end

    describe "Con informacion invalida" do
      before { click_button "Guardar cambios"}
      it{should have_content('error')}
    end

    describe "Con informacion valida" do
      let(:nuevo_nombre)  { "Nuevo nombre" }
      let(:nuevo_email) { "nuevo@example.com" }
      before do
        fill_in "Nombre",             with: nuevo_nombre
        fill_in "Email",            with: nuevo_email
        fill_in "Password",         with: usuario.password
        fill_in "Confirmacion", with: usuario.password
        click_button "Guardar cambios"
      end

      it { should have_title(nuevo_nombre) }
      it { should have_selector('div.alert.alert-success') }
      it { should have_link('Salir', href: salida_path) }
      specify { expect(usuario.reload.nombre).to  eq nuevo_nombre }
      specify { expect(usuario.reload.email).to eq nuevo_email }
    end
  end

  describe "siguiendo/seguidores" do
    let(:usuario) { FactoryGirl.create(:usuario) }
    let(:otro_usuario) { FactoryGirl.create(:usuario) }
    before { usuario.seguir!(otro_usuario) }

    describe "usuarios seguido" do
      before do
        sign_in usuario
        visit siguiendo_usuario_path(usuario)
      end

      it { should have_title(full_titulo('Siguiendo')) }
      it { should have_selector('h3', text: 'Siguiendo') }
      it { should have_link(otro_usuario.nombre, href: usuario_path(otro_usuario)) }
    end

    describe "seguidores" do
      before do
        sign_in otro_usuario
        visit seguidores_usuario_path(otro_usuario)
      end

      it { should have_title(full_titulo('Seguidores')) }
      it { should have_selector('h3', text: 'Seguidores') }
      it { should have_link(usuario.nombre, href: usuario_path(usuario)) }
    end
  end

end
