require 'spec_helper'

describe Relacion do
  
  let(:seguidor) { FactoryGirl.create(:usuario)}
  let(:seguido)  { FactoryGirl.create(:usuario)}
  let(:relacion) { seguidor.relacions.build(seguido_id: seguido.id)}

  subject {relacion}

  it {should be_valid}

  describe "metodos seguidor" do
    it { should respond_to(:seguidor) }
    it { should respond_to(:seguido) }
    its(:seguidor) { should eq seguidor }
    its(:seguido) { should eq seguido }
  end

  describe "cuando el seguido no esta presente" do
    before { relacion.seguido_id = nil }
    it { should_not be_valid }
  end

  describe "cuando el seguidor no esta presente" do
    before { relacion.seguidor_id = nil }
    it { should_not be_valid }
  end
end
