require 'spec_helper'

describe Micropost do
  let(:usuario) { FactoryGirl.create(:usuario) }
  before do
    @micropost = usuario.microposts.build(content: "Lorem ipsum")
  end

  subject { @micropost }

  it { should respond_to(:content) }
  it { should respond_to(:usuario_id) }
  it { should respond_to(:usuario) }
  its(:usuario) { should eq usuario }

  it { should be_valid }

  describe "cuando usuario_id no esta presente" do
    before { @micropost.usuario_id = nil }
    it { should_not be_valid }
  end

  describe "con contenido en blanco" do
    before { @micropost.content = " " }
    it { should_not be_valid }
  end

  describe "con contenido que es muy largo" do
    before { @micropost.content = "a" * 141 }
    it { should_not be_valid }
  end

end
