require 'spec_helper'

describe Usuario do
  before { @usuario = Usuario.new(nombre: "Ronald Gomez", email: "ronald.gomez@example.com",
  									password: "foobar", password_confirmation: "foobar")}
  subject { @usuario }

  it { should respond_to(:nombre)}
  it { should respond_to(:email)}
  it { should respond_to(:password_digest)}
  it { should respond_to(:password)}
  it { should respond_to(:password_confirmation)}
  it { should respond_to(:remember_token) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:admin) }
  it { should respond_to(:microposts) }
  it { should respond_to(:feed) }
  it { should respond_to(:relacions) }
  it { should respond_to(:usuarios_seguido)}
  it { should respond_to(:relacions_inversa) }
  it { should respond_to(:seguidores) }
  it { should respond_to(:siguiendo?) }
  it { should respond_to(:seguir!) }
  it { should respond_to(:noseguir!) }

  it {should be_valid}
  it { should_not be_admin }

  describe "con atributo admin establecido 'true'" do
    before do
      @usuario.save!
      @usuario.toggle!(:admin)
    end

    it { should be_admin }
  end

  describe "Cuando el nombre no esta presente" do
  	before {@usuario.nombre = " "}
  	it { should_not be_valid}
  end

  describe "Cuando el email no esta presente" do
  	before { @usuario.email = " "}
  	it { should_not be_valid}
  end

  describe "Cuando el nombre es muy largo" do
  	before { @usuario.nombre = "a" * 51}
  	it {should_not be_valid}
  end

  describe "Cuando el formato de email no es valido" do
  	it "Deberia ser valido" do
  		direcciones =  %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
  		direcciones.each do |direccion_valida|
  			@usuario.email = direccion_valida
  			expect(@usuario).to be_valid
  		end
  	end
  end

  describe "Cuando el email ya ha sido tomado" do
  	before do
  		user_with_same_email = @usuario.dup
  		user_with_same_email.email = @usuario.email.upcase
  		user_with_same_email.save
  	end

  	it {should_not be_valid}
  end

  describe "Cuando el password no esta presente" do
  	before do
  		@usuario = Usuario.new(nombre: "Usuario de ejemplo", email:"usuario@example.com", password: " ", password_confirmation:" ")
  	end
  	it {should_not be_valid}
  end

  describe "Cuando el password no es igual a la confimaciòn" do
  	before { @usuario.password_confirmation = "diferente"}
  	it {should_not be_valid}
  end

  describe "Con un password que es corto" do
  	before {@usuario.password = @usuario.password_confirmation = "a"*5}
  	it { should_not be_valid}
  end

  describe "Retorna el valor de un metodo de autenticacion" do
  	before {@usuario.save}
  	let(:found_user) {Usuario.find_by(email: @usuario.email)}

  	describe "Con un password valido" do
  		it {should eq found_user.authenticate(@usuario.password)}
  	end

  	describe "Con un password invalido" do
  		let(:user_for_invalid_password) {found_user.authenticate("invalido")}
  		it {should_not eq user_for_invalid_password}
  		specify { expect(user_for_invalid_password).to be_false}
  	end
  end

  describe "remember token" do
    before { @usuario.save }
    its(:remember_token) { should_not be_blank }
  end

  describe "asociaciones micropost" do

    before { @usuario.save }
    let!(:older_micropost) do
      FactoryGirl.create(:micropost, usuario: @usuario, created_at: 1.day.ago)
    end
    let!(:newer_micropost) do
      FactoryGirl.create(:micropost, usuario: @usuario, created_at: 1.hour.ago)
    end

    it "deberia tener el adecuado micropost en el orden correcto" do
      expect(@usuario.microposts.to_a).to eq [newer_micropost, older_micropost]
    end

    it "deberia destruir microposts asociados" do
      microposts = @usuario.microposts.to_a
      @usuario.destroy
      expect(microposts).not_to be_empty
      microposts.each do |micropost|
        expect(Micropost.where(id: micropost.id)).to be_empty
      end
    end

    describe "estado" do
      let(:unfollowed_post) do
        FactoryGirl.create(:micropost, usuario: FactoryGirl.create(:usuario))
      end

      let(:usuario_seguido) { FactoryGirl.create(:usuario) }

      before do
        @usuario.seguir!(usuario_seguido)
        3.times { usuario_seguido.microposts.create!(content: "Lorem ipsum") }
      end

      its(:feed) { should include(newer_micropost) }
      its(:feed) { should include(older_micropost) }
      its(:feed) { should_not include(unfollowed_post) }
      its(:feed) do
        usuario_seguido.microposts.each do |micropost|
          should include(micropost)
        end
      end
    end
  end

  describe "siguiendo" do
    let(:otro_usuario) { FactoryGirl.create(:usuario) }
    before do
      @usuario.save
      @usuario.seguir!(otro_usuario)
    end

    it { should be_siguiendo(otro_usuario) }
    its(:usuarios_seguido) { should include(otro_usuario) }

    describe "usuario seguido" do
      subject { otro_usuario }
      its(:seguidores) { should include(@usuario) }
    end

    describe "y nosiguiendo" do
      before { @usuario.noseguir!(otro_usuario) }

      it { should_not be_siguiendo(otro_usuario) }
      its(:usuarios_seguido) { should_not include(otro_usuario) }
    end
  end
end
