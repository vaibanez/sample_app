def full_titulo(titulo_pagina)
  titulo_basico="Tutorial Ruby on Rails, Aplicacion de Ejemplo"
  if titulo_pagina.empty?
  	titulo_basico
  else
  	"#{titulo_basico} | #{titulo_pagina}"
  end
end

def sign_in(usuario, options={})
  if options[:no_capybara]
    remember_token = Usuario.new_remember_token
    cookies[:remember_token] = remember_token
    usuario.update_attribute(:remember_token, Usuario.digest(remember_token))
    #self.current_user = usuario
  else
    visit ingreso_path
    fill_in "Email", with: usuario.email
    fill_in "Password", with: usuario.password
    click_button "Ingresar"
  end
end

def signed_in?
  !current_user.nil?
end

def sign_out
  current_user.update_attribute(:remember_token,
                                  Usuario.digest(Usuario.new_remember_token))
  cookies.delete(:remember_token)
  self.current_user = nil
end

def current_user=(usuario)
  @current_user = usuario
end

def current_user
  remember_token = Usuario.digest(cookies[:remember_token])
  @current_user ||= Usuario.find_by(remember_token: remember_token)
end

def current_user?(usuario)
    usuario == current_user
end

def signed_in_user
  unless signed_in?
    store_location
    redirect_to ingreso_url, notice: "Please sign in."
  end
end

def redirect_back_or(default)
  redirect_to(session[:return_to] || default)
  session.delete(:return_to)
end

def store_location
  session[:return_to] = request.url if request.get?
end