require 'spec_helper'

describe RelacionsController do

  let(:usuario) { FactoryGirl.create(:usuario) }
  let(:otro_usuario) { FactoryGirl.create(:usuario) }

  before { sign_in usuario, no_capybara: true }

  describe "creando una relacion con Ajax" do

    it "debe incrementar el conteo de la relacion" do
      expect do
        xhr :post, :create, relacion: { seguido_id: otro_usuario.id }
      end.to change(Relacion, :count).by(1)
    end

    it "debe responder con satisfaccion" do
      xhr :post, :create, relacion: { seguido_id: otro_usuario.id }
      expect(response).to be_success
    end
  end

  describe "destruyendo una relacion con Ajax" do

    before { usuario.seguir!(otro_usuario) }
    let(:relacion) do
      usuario.relacions.find_by(seguido_id: otro_usuario.id)
    end

    it "debe disminuir el conteo de la relacion" do
      expect do
        xhr :delete, :destroy, id: relacion.id
      end.to change(Relacion, :count).by(-1)
    end

    it "debe responder con satisfaccion" do
      xhr :delete, :destroy, id: relacion.id
      expect(response).to be_success
    end
  end
end