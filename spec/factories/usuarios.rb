# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :usuario do
    #nombre "Ronald Gomez"
    #email "rgomez@example.com"
    sequence(:nombre)  { |n| "Persona #{n}" }
    sequence(:email) { |n| "persona_#{n}@example.com"}
    password "foobar"
    password_confirmation "foobar"
    
    factory :admin do
      admin true
    end
  end
end
