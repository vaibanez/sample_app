class UsuariosController < ApplicationController
  before_action :usuario_ingresado, only: [:index, :edit, :update, :destroy, :siguiendo, :seguidores]
  before_action :usuario_correcto,  only: [:edit, :update]
  before_action :usuario_administrador,     only: :destroy

  def index
    @usuarios = Usuario.paginate(page: params[:page])
  end

  def new
  	@usuario = Usuario.new
  end
  def show
  	@usuario = Usuario.find(params[:id])
    @microposts = @usuario.microposts.paginate(page: params[:page])
  end
  def create
  	@usuario = Usuario.new(usuario_parametros)
  	if @usuario.save
      sign_in @usuario
  		flash[:success] = "Bienvenido a la aplicacion de ejemplo"
  		redirect_to @usuario
  	else
  		render 'new'
  	end
  end

  def edit
    #@usuario = Usuario.find(params[:id])
  end

  def update
    @usuario = Usuario.find(params[:id])
    if @usuario.update_attributes(usuario_parametros)
      flash[:success] = "Perfil actualizado"
      redirect_to @usuario
    else
      render 'edit'
    end
  end

  def destroy
    Usuario.find(params[:id]).destroy
    flash[:success] = "Usuario borrado."
    redirect_to usuarios_url
  end

  def siguiendo
    @titulo = "Siguiendo"
    @usuario = Usuario.find(params[:id])
    @usuarios = @usuario.usuarios_seguido.paginate(page: params[:page])
    render 'mostrar_seguidor'
  end

  def seguidores
    @titulo = "Seguidores"
    @usuario = Usuario.find(params[:id])
    @usuarios = @usuario.seguidores.paginate(page: params[:page])
    render 'mostrar_seguidor'
  end

  private

  	def usuario_parametros
  		params.require(:usuario).permit(:nombre, :email, :password, :password_confirmation)
  	end

    def usuario_ingresado
      unless signed_in?
        store_location
        redirect_to ingreso_url, notice: "Por favor ingrese."
      end
    end

    def usuario_correcto
      @usuario = Usuario.find(params[:id])
      redirect_to(root_url) unless current_user?(@usuario)
    end

    def usuario_administrador
      redirect_to(root_url) unless current_user.admin?
    end
end
