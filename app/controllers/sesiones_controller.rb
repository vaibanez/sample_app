class SesionesController < ApplicationController
  def new
  end

  def create
  	usuario = Usuario.find_by(email: params[:sesion][:email].downcase)
    if usuario && usuario.authenticate(params[:sesion][:password])
      # Sign the user in and redirect to the user's show page.}
      sign_in usuario
      #redirect_to usuario
      redirect_back_or usuario
    else
      flash.now[:error] = 'Email o password invalido' # Not quite right!
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end
end
