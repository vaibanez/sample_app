class Usuario < ActiveRecord::Base
  has_many :microposts, dependent: :destroy
  has_many :relacions, foreign_key: "seguidor_id", dependent: :destroy
  has_many :usuarios_seguido, through: :relacions, source: :seguido
  has_many :relacions_inversa, foreign_key: "seguido_id",
                                   class_name:  "Relacion",
                                   dependent:   :destroy
  has_many :seguidores, through: :relacions_inversa, source: :seguidor

	before_save { self.email = email.downcase}
	before_create :create_remember_token
	validates :nombre, presence: true, length: {maximum: 50}
	VALID_EMAIL_REGEX= /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
	has_secure_password
	validates :password, length: {minimum: 6}

  def feed
    Micropost.from_usuarios_seguido_by(self)
  end

  def siguiendo?(otro_usuario)
    relacions.find_by(seguido_id: otro_usuario.id)
  end

  def seguir!(otro_usuario)
    relacions.create!(seguido_id: otro_usuario.id)
  end

  def noseguir!(otro_usuario)
    relacions.find_by(seguido_id: otro_usuario.id).destroy
  end
  
	def Usuario.new_remember_token
  	SecureRandom.urlsafe_base64
  end

  def Usuario.digest(token)
   	Digest::SHA1.hexdigest(token.to_s)
  end

  private

    def create_remember_token
      self.remember_token = Usuario.digest(Usuario.new_remember_token)
    end
end
