class Micropost < ActiveRecord::Base
  belongs_to :usuario
  default_scope -> { order('created_at DESC') }
  validates :content, presence: true, length: { maximum: 140 }
  validates :usuario_id, presence: true

  def self.from_usuarios_seguido_by(usuario)
    usuarios_seguido_ids = usuario.usuarios_seguido_ids
    #where("usuario_id IN (?) OR usuario_id = ?", usuarios_seguido_ids, usuario)
    where("usuario_id IN (:usuarios_seguido_ids) OR usuario_id = :usuario_id",
          usuarios_seguido_ids: usuarios_seguido_ids, usuario_id: usuario)
    # usuarios_seguido_ids = "SELECT relacions.seguido_id FROM relacions
    #                      WHERE relacions.seguido_id = :usuario_id"
    # where("usuario_id IN (#{usuarios_seguido_ids}) OR usuario_id = :usuario_id",
    #       usuario_id: usuario.id)
  end
end
