module ApplicationHelper
  def full_titulo(titulo_pagina)
  	titulo_basico="Tutorial Ruby on Rails, Aplicacion de Ejemplo"
  	if titulo_pagina.empty?
  		titulo_basico
  	else
  		"#{titulo_basico} | #{titulo_pagina}"
  	end
  end
end
