module SesionesHelper
  def sign_in(usuario)
    remember_token = Usuario.new_remember_token
    cookies.permanent[:remember_token] = remember_token
    usuario.update_attribute(:remember_token, Usuario.digest(remember_token))
    self.current_user = usuario
  end

  def signed_in?
    !current_user.nil?
  end

  def sign_out
    current_user.update_attribute(:remember_token,
                                  Usuario.digest(Usuario.new_remember_token))
    cookies.delete(:remember_token)
    self.current_user = nil
  end

  def current_user=(usuario)
    @current_user = usuario
  end

  def current_user
    remember_token = Usuario.digest(cookies[:remember_token])
    @current_user ||= Usuario.find_by(remember_token: remember_token)
  end

  def current_user?(usuario)
    usuario == current_user
  end

  def signed_in_user
    unless signed_in?
      store_location
      redirect_to ingreso_url, notice: "Please sign in."
    end
  end

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    session.delete(:return_to)
  end

  def store_location
    session[:return_to] = request.url if request.get?
  end
end
